SQL Authentication
By Bèr Kessels
sponsored by sympal.nl
developed for sympal (sympal.nl)

SQL authentication is a module that uses the external authentication methods in Drupal to connect to any other database. 
Whether that is a PHPbb MySQL database, or another drupal site in postgresql does not really matter. It is fully configuratble and easy to use. 
Its user-end system is similar to the drupalID, with one difference: your data will not go unencrypted over the big bad web.